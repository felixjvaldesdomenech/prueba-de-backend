const mysql = require('mysql');

const connection = mysql.createConnection({
  host: '127.0.0.1',
  user: '',
  password: '',
  database: 'Felix Valdes'
});

connection.connect();

function obtenerDatosPorId(id) {
  return new Promise((resolve, reject) => {
    connection.query('SELECT * FROM tu_tabla WHERE id = ?', [id], (error, results) => {
      if (error) {
        reject(error);
      } else {
        resolve(results);
      }
    });
  });
}

function obtenerTodosLosDatos() {
  return new Promise((resolve, reject) => {
    connection.query('SELECT * FROM tu_tabla', (error, results) => {
      if (error) {
        reject(error);
      } else {
        resolve(results);
      }
    });
  });
}

function darDeAltaDatos(datos) {
  return new Promise((resolve, reject) => {
    connection.query('INSERT INTO tu_tabla SET ?', datos, (error, results) => {
      if (error) {
        reject(error);
      } else {
        resolve(results);
      }
    });
  });
}

function darDeBajaDatos(id) {
  return new Promise((resolve, reject) => {
    connection.query('DELETE FROM tu_tabla WHERE id = ?', [id], (error, results) => {
      if (error) {
        reject(error);
      } else {
        resolve(results);
      }
    });
  });
}

function modificarDatos(id, nuevosDatos) {
  return new Promise((resolve, reject) => {
    connection.query('UPDATE tu_tabla SET ? WHERE id = ?', [nuevosDatos, id], (error, results) => {
      if (error) {
        reject(error);
      } else {
        resolve(results);
      }
    });
  });
}

module.exports = {
  obtenerDatosPorId,
  obtenerTodosLosDatos,
  darDeAltaDatos,
  darDeBajaDatos,
  modificarDatos
};