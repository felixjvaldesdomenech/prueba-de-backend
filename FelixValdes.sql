CREATE TABLE FelixValdes (
    id INT(10),
    contenido VARCHAR(100),
    PRIMARY KEY (id)

);
const mysql = require('mysql');

const connection = mysql.createConnection({
  host: '127.0.0.1',
  user: '',
  password: '',
  database: 'Felix Valdes'
});

connection.connect((error) => {
  if (error) {
    console.error('Error al conectar a la base de datos: ', error);
    return;
  }
  console.log('Conexión establecida con la base de datos MySQL');
});

module.exports = connection;