const mysql = require('mysql');

// Configuración de la conexión a la base de datos
const connection = mysql.createConnection({
  host: '127.0.0.1',
  user: '',
  password: '',
  database: 'Felix Valdes'
});

// Obtener datos por ID
const getById = (req, res) => {
  const { id } = req.params;

  connection.query('SELECT * FROM tu_tabla WHERE id = ?', [id], (err, results) => {
    if (err) {
      res.status(500).json({ error: 'Error al obtener los datos por ID' });
    } else {
      res.json(results);
    }
  });
};

// Obtener todos los datos
const getAll = (req, res) => {
  connection.query('SELECT * FROM tu_tabla', (err, results) => {
    if (err) {
      res.status(500).json({ error: 'Error al obtener todos los datos' });
    } else {
      res.json(results);
    }
  });
};

// Realizar alta
const create = (req, res) => {
  const newRecord = req.body;

  connection.query('INSERT INTO tu_tabla SET ?', newRecord, (err, result) => {
    if (err) {
      res.status(500).json({ error: 'Error al realizar el alta' });
    } else {
      res.json({ message: 'Alta realizada correctamente', id: result.insertId });
    }
  });
};

// Realizar modificación
const update = (req, res) => {
  const { id } = req.params;
  const updatedRecord = req.body;

  connection.query('UPDATE tu_tabla SET ? WHERE id = ?', [updatedRecord, id], (err, result) => {
    if (err) {
      res.status(500).json({ error: 'Error al realizar la modificación' });
    } else {
      res.json({ message: 'Modificación realizada correctamente' });
    }
  });
};

// Realizar baja
const remove = (req, res) => {
  const { id } = req.params;

  connection.query('DELETE FROM tu_tabla WHERE id = ?', [id], (err, result) => {
    if (err) {
      res.status(500).json({ error: 'Error al realizar la baja' });
    } else {
      res.json({ message: 'Baja realizada correctamente' });
    }
  });
};

module.exports = {
  getById,
  getAll,
  create,
  update,
  remove
};
